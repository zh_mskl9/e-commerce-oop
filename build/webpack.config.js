const path = require('path');

const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');

const paths = require('./paths');
const helpers = require('./helpers');

const { NODE_ENV } = process.env;

const isProduction = NODE_ENV === 'production';

module.exports = {
  entry: paths.entry,
  mode: NODE_ENV,
  output: {
    path: paths.dist,
    filename: isProduction ? 'bundle.[hash].min.js' : 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader',
          {
            loader: 'sass-resources-loader',
            options: {
              resources: helpers.provideSassResources(),
            },
          },
        ],
      },
      {
        test: /\.pug$/,
        use: {
          loader: 'pug-loader',
          options: {
            root: paths.src,
            pretty: true,
          },
        },
      },
      {
        test: /\.(png|jpe?g|gif|svg|webp|ico)$/,
        use: {
          loader: 'file-loader',
          options: {
            regExp: /(?<=img)((?:[a-z0-9\-\_\.\/]+\/)|(?:\/))([a-z0-9\-\_\.]+)\.(png|jpg|gif|webp)$/i,
            name: '[name].[ext]',
            outputPath: 'img',
          },
        },
      },
      {
        test: /\.(eot|ttf|woff|woff2|otf)(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts',
          },
        },
      },
    ],
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
      }),
      new OptimizeCssAssetsPlugin({}),
    ],
  },
  devServer: {
    host: '0.0.0.0',
    port: 7000,
  },
  resolve: {
    alias: {
      '@': paths.src,
      '@img': path.resolve(paths.src, 'assets/img'),
      '@design': path.resolve(paths.src, 'assets/scss/main.scss'),
    },
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: isProduction ? '[name].[hash].min.css' : '[name].css',
      chunkFilename: isProduction ? '[id].[hash].css' : '[id].css',
    }),
    new SVGSpritemapPlugin('src/assets/img/icons/*.svg', {
      output: {
        filename: 'img/spritemap.svg',
      },
    }),
    ...helpers.generateHtmlPlugins(path.resolve(paths.src, 'pages')),
  ],
};
